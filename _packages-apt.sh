#!/bin/bash

# Declare packages

declare -a _packages=(
    aptitude
    apt-transport-https
    etherwake
    vlc-bin
    build-essential
    curl
    file
    git
    cmake
    make
    wget curl
    terminator
    virtualenv
    pass
    gpg gpg-agent
    sshpass sshuttle
    ecryptfs-utils
    llvm
    libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev libffi-dev liblzma-dev libncurses5-dev libncursesw5-dev
    xz-utils tk-dev  python-openssl
)

# Installation
sudo apt update -q
sudo apt upgrade -y -q
sudo apt install -y -q ${_packages[@]}
sudo apt autoclean
